---
layout: markdown_page
title: "FY22-Q3 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q3. Learn more here!"
canonical_path: "/company/okrs/fy22-q3/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from August 1, 2021 to October 31, 2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-06-28 | CEO shares top goals with E-group for feedback |
| -5 | 2021-06-28 | CEO pushes top goals to this page |
| -3 | 2021-07-12 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel|
| -2 | 2021-07-19 | E-group 50 minute draft review meeting |
| -2 | 2021-07-19 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-07-26 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-08-02 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: GitLab Managed future 
   1. **CEO KR:** X% of [features missing on GitLab.com](/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/#all-differences-between-gitlab-saas-and-self-managed) have moved to scoped or [viable](/direction/maturity/)
   1. **CEO KR:** Launch first customer facing iteration of three new delivery methods: Horse, Plus and Region
   1. **CEO KR:** Have Workspace replace the admin screen

### 2. CEO: Conversion from free 
   1. **CEO KR:** Cloud licensing for 100% of new and renewing subscriptions
   1. **CEO KR:** Release has a wizard with 100 templates
   1. **CEO KR:** 100% of sales people months have 2 [sales accepted opportunities (SAOs)](/handbook/marketing/revenue-marketing/sdr/#qualification-criteria-and-saos). For example, if you had 10 sales people, you would have 30 sales people months in quarter (10 people x 3 months in the quarter). If two people didn’t get 2 SAOs in the first month and one person didn’t get 2 SAOs in the next, you would have achieved 90% of the target (27 sales people months out of 30 total sales people months) for the quarter.
   1.  **CEO KR:** Increase Trial conversion to X%, Increase Free to Trial/Paid to X%.

### 3. CEO: Even prouder to work here
   1. **CEO KR:** KR: 90% of team members answering an internal survey say that they agree that:
      1. They are bullish on GitLab’s long-term outlook
      1. Feel comfortable delivering our Golden Pitch
      1. Feel comfortable speaking to our brand message
      1. [If attending] Contribute was a success
      1. We are at mile 3 or 4 of a marathon or ultra-marathon
      1. Love the new GitLab Brand
      1. GitLab pays competitively
   1. **CEO KR:** Meet hiring targets and have a diverse top of pipeline
   1. **CEO KR:** 7 certifications with more than 10,000 certificates issues
   1. **CEO KR:** Graduate Project Gotham and Project Sycamore from internal limited to public
